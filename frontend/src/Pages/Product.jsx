import React, { useContext } from 'react'
import { useParams } from "react-router-dom";
import { ShopContext } from '../Context/ShopContext'
import Breadcrum from '../Components/Breadcrums/Breadcrum';
import ProductDisplay from '../Components/ProductDisplay/ProductDisplay';
import DescriptionBox from '../Components/DescriptionBox/DescriptionBox';
import RelatedProducts from '../Components/RelatedProducts/RelatedProducts';

const Product = () => {

  const {all_product} = useContext(ShopContext);                     //  <---get all products
  const {productId} = useParams();                                   //  <---by this Id will find the product in all products
  const product = all_product.find((e) =>                           //  <---All information about product whose ID is match
    e.id === Number(productId)                                       //  <---will get productId in String format so convert in into Number 
  )                                                                 //  <---will check all_product ID and value in productId same then assign this Id to this variable and get all information about the product this Id

  return (
    <div>
      <Breadcrum product={product}/>
      <ProductDisplay product={product}/>
      <DescriptionBox/>
      <RelatedProducts/>
    </div>
  )
}

export default Product
