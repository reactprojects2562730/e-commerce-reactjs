import React from "react";
import "./DescriptionBox.css";

const DescriptionBox = () => {
  return (
    <div className="descriptionbox">
      <div className="descriptionbox-navigator">
        <div className="descriptionbox-nav-box">Description</div>
        <div className="descriptionbox-nav-box fade">Reviews (122)</div>
      </div>
      <div className="descriptionbox-description">
        <p>
          An e-commerce website is an inline platform that facilitate buying and
          selling of products or services over the internet serves as virtual
          market place where indual and businesses shoecase their products,
          intract with customers, and conduct transition without need for
          phycial presence.
        </p>
        <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Est nihil dolorem, quod nisi commodi nulla.</p>
      </div>
    </div>
  );
};

export default DescriptionBox;
